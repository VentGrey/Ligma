# PLEASE READ THE MAN PAGE BEFORE EDITING THIS FILE!
# https://htmlpreview.github.io/?https://github.com/conformal/spectrwm/blob/master/spectrwm.html
# NOTE: all rgb color values in this file are in hex! see XQueryColor for examples

 workspace_limit	= 10
 focus_mode		= default
 focus_close		= previous
 focus_close_wrap	= 1
 focus_default		= last
 spawn_position		= next
 workspace_clamp	= 1
 warp_focus		= 1
 warp_pointer		= 1

# Window Decoration
 border_width		 = 1
 color_focus		 = rgb:89/b4/fa
 color_focus_maximized	 = rgb:f2/cd/cd
 color_unfocus		 = rgb:cb/a6/f7
 color_unfocus_maximized = rgb:88/88/00
 region_padding	         = 4
 tile_gap                = 4

# Region containment
# Distance window must be dragged/resized beyond the region edge before it is
# allowed outside the region.
 boundary_width 		= 50

# Remove window border when bar is disabled and there is only one window in workspace
 disable_border		= 1

# Bar Settings
 bar_enabled		= 1
 bar_enabled_ws[1]	= 1
 bar_border_width	= 1
 bar_border[1]		= rgb:1e/1e/2e
 bar_border_unfocus[1]	= rgb:00/40/40
 bar_color[1]		= rgb:11/11/1b
 bar_color_selected[1]	= rgb:00/80/80
 bar_font_color[1]	= rgb:cd/d6/f4,rgb:cb/a6/f7,rgb:89/b4/fa,rgb:f3/8b/a8,rgb:f2/cd/cd,rgb:a6/e3/a1,rgb:f9/e2/af,rgb:fa/b3/87,rgb:94/e2/d5
 bar_font_color_selected	= black
 bar_font		= JetBrainsMono Nerd Font:size=8,Clear Sans:size=8
 bar_font_pua		= JetBrainsMono Nerd Font:size=8:antialias=true,Noto Color Emoji:size=8
 bar_action		= ~/.config/spectrwm/ligma.pl
 bar_action_expand	= 1
 bar_justify		= left
 bar_format		= +N:+I +S [ +L ] [+30W] +50< +|1R +@fg=2; 󰥔 %A %d %b (%R)+@fg=0; +A
 workspace_indicator	= listcurrent,listactive,listempty,markcurrent,markactive,markurgent
 workspace_mark_active = "+@fg=4;^+@fg=0;"
 workspace_mark_current = "+@fg=5;*+@fg=0;"
 workspace_mark_urgent = "+@fg=3;^+@fg=0;"
 bar_at_bottom		= 0
 stack_enabled		= 1
 clock_enabled		= 1
 clock_format		= %a %b %d %R
 iconic_enabled	= 1
 maximize_hide_bar	= 1
 window_class_enabled	= 1
 window_instance_enabled	= 1
 window_name_enabled	= 1
 verbose_layout		= 1
 urgent_enabled		= 1
 urgent_collapse	= 0

# Dialog box size ratio when using TRANSSZ quirk; 0.3 < dialog_ratio <= 1.0
 dialog_ratio		= 0.6

# Split a non-RandR dual head setup into one region per monitor
# (non-standard driver-based multihead is not seen by spectrwm)
 region		= screen[1]:1920x1080+0+0
# region		= screen[1]:1280x1024+1280+0

# Launch applications in a workspace of choice
# autorun		= ws[1]:xterm
# autorun		= ws[2]:xombrero http://www.openbsd.org

# Customize workspace layout at start
# layout		= ws[1]:4:0:0:0:vertical
# layout		= ws[2]:0:0:0:0:horizontal
# layout		= ws[3]:0:0:0:0:fullscreen
# layout		= ws[4]:4:0:0:0:vertical_flip
# layout		= ws[5]:0:0:0:0:horizontal_flip

# Set workspace name at start
 name			= ws[1]:
 name			= ws[2]:
 name			= ws[3]:
 name			= ws[4]:󰇧
 name			= ws[5]:󰽱
 name			= ws[6]:
 name			= ws[7]:󰵅
 name			= ws[8]:
 name			= ws[9]:
 name			= ws[10]:

# Mod key, (Windows key is Mod4) (Apple key on OSX is Mod2)
 modkey = Mod4

# This allows you to include pre-defined key bindings for your keyboard layout.
# keyboard_mapping = ~/.spectrwm_us.conf

# PROGRAMS

# Validated default programs:
 program[lock]		= i3lock-fancy
 program[term]		= tilix
 program[menu]		= rofi -show drun
# program[search]	= dmenu $dmenu_bottom -i -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected
# program[name_workspace]	= dmenu $dmenu_bottom -p Workspace -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected

# To disable validation of the above, free the respective binding(s):
# bind[]		= MOD+Shift+Delete	# disable lock
# bind[]		= MOD+Shift+Return	# disable term
# bind[]		= MOD+p			# disable menu

# Optional default programs that will only be validated if you override:
 program[screenshot_all]	= ~/.config/spectrwm/screenshot-tool.sh 	# optional
# program[screenshot_wind]	= screenshot.sh window	# optional
# program[initscr]	= initscreen.sh			# optional

# EXAMPLE: Define 'firefox' action and bind to key.
# program[firefox]	= firefox http://spectrwm.org/
# bind[firefox]		= MOD+Shift+b

# QUIRKS
# Default quirks, remove with: quirk[class:name] = NONE
# quirk[MPlayer:xv]			= FLOAT + FULLSCREEN + FOCUSPREV
# quirk[OpenOffice.org 2.4:VCLSalFrame]	= FLOAT
# quirk[OpenOffice.org 3.0:VCLSalFrame]	= FLOAT
# quirk[OpenOffice.org 3.1:VCLSalFrame]	= FLOAT
 quirk[Firefox-bin:firefox-bin]		= TRANSSZ
 quirk[Firefox:Dialog]			= FLOAT
 quirk[Librewolf:librewolf]		= TRANSSZ
 quirk[Librewolf:Dialog]			= FLOAT
 quirk[trayer] = FLOAT + ANYWHERE + NOFOCUSCYCLE + MINIMALBORDER + NOFOCUSONMAP
# quirk[Gimp:gimp]			= FLOAT + ANYWHERE
# quirk[XTerm:xterm]			= XTERM_FONTADJ
# quirk[xine:Xine Window]			= FLOAT + ANYWHERE
# quirk[Xitk:Xitk Combo]			= FLOAT + ANYWHERE
# quirk[xine:xine Panel]			= FLOAT + ANYWHERE
# quirk[Xitk:Xine Window]			= FLOAT + ANYWHERE
# quirk[xine:xine Video Fullscreen Window] = FULLSCREEN + FLOAT
# quirk[pcb:pcb]				= FLOAT

# Clear all key definitions except for ours. (Even the defaults)
keyboard_mapping = /dev/null

# Terminal
bind[term]		= MOD+Shift+Return

# Keybindings jsjsjsjs
bind[cycle_layout] = MOD+Control+l
bind[bar_toggle]	= MOD+b
bind[bar_toggle_ws]	= MOD+Shift+b

# Cycle through workspaces
bind[ws_1] = MOD+1
bind[ws_2] = MOD+2
bind[ws_3] = MOD+3
bind[ws_4] = MOD+4
bind[ws_5] = MOD+5
bind[ws_6] = MOD+6
bind[ws_7] = MOD+7
bind[ws_8] = MOD+8
bind[ws_9] = MOD+9
bind[ws_10] = MOD+0

bind[mvws_1]		= MOD+Shift+1
bind[mvws_2]		= MOD+Shift+2
bind[mvws_3]		= MOD+Shift+3
bind[mvws_4]		= MOD+Shift+4
bind[mvws_5]		= MOD+Shift+5
bind[mvws_6]		= MOD+Shift+6
bind[mvws_7]		= MOD+Shift+7
bind[mvws_8]		= MOD+Shift+8
bind[mvws_9]		= MOD+Shift+9
bind[mvws_10]		= MOD+Shift+0

bind[wind_del] = MOD+Shift+q

bind[width_grow] = MOD+Shift+Left
bind[width_shrink] = MOD+Shift+Right
bind[height_grow] = MOD+Shift+Up
bind[height_shrink] = MOD+Shift+Down

bind[lock] = MOD+Shift+l

bind[maximize_toggle] = MOD+f

bind[] = MOD+Shift+x
bind[quit] = MOD+Shift+x
bind[restart] = MOD+Shift+r

# Program Keybindings
# Dmenu app menu
bind[menu] = Mod+space

# RoFi scripts
program[rofi-power] = ~/.config/spectrwm/power-menu 
bind[rofi-power] = MOD+p

# Emacs
program[emacs] = emacsclient -c 
bind[emacs] = MOD+Shift+e

# Logout
program[logout] = loginctl kill-session $XDG_SESSION_ID
bind[logout] = MOD+Shift+x

# Volume
program[raise_volume] = pamixer -i 5
bind[raise_volume] = XF86AudioRaiseVolume

program[lower_volume] = pamixer -d 5
bind[lower_volume] = XF86AudioLowerVolume

program[mute_volume] = pactl set-sink-mute 0 toggle
bind[mute_volume] = XF86AudioMute

program[mute_mic] = pactl set-source-mute 1 toggle
bind[mute_mic] = XF86AudioMicMute

# Backlight
program[increase_backlight] = light -U 5
bind[increase_backlight] = XF86MonBrightnessDown

program[decrease_backlight] = light -A 5
bind[decrease_backlight] = XF86MonBrightnessUp